exports.parseIdsFromJson = function (countyJson) {
	var countyIds = [];
	for (var i = 0; i < countyJson.length; i++) {
		countyIds.push(countyJson[i].id);
	}
	return countyIds;
}

exports.parseSearchModel = function (vehicleSearch, countyIds) {
	var make = (vehicleSearch.make != null ? ' make = "' + vehicleSearch.make + '"' : ' make LIKE "%"');
	var model = (vehicleSearch.model != null ? ' AND model = "' + vehicleSearch.model + '"' : '');
	var bodyType = (vehicleSearch.bodyType != null ? ' AND body_style = "' + vehicleSearch.bodyType + '"' : '');
	var fuelType = (vehicleSearch.fuelType != null ? ' AND fuel_type = "' + vehicleSearch.fuelType + '"' : '');
	var transmission = (vehicleSearch.transmission != null ? ' AND transmission = "' + vehicleSearch.transmission + '"' : '');
	var minPrice = (vehicleSearch.minPrice > 0 ? ' AND price >= ' + vehicleSearch.minPrice : '');
	var maxPrice = (vehicleSearch.maxPrice > 0 ? ' AND price <= ' + vehicleSearch.maxPrice : '');
	var minYear = (vehicleSearch.minYear > 0 ? ' AND year >= ' + vehicleSearch.minYear : '');
	var maxYear = (vehicleSearch.maxYear > 0 ? ' AND year <= ' + vehicleSearch.maxYear : '');
	
	var searchString = 'SELECT vehicle.vehicle_id, year, make, model, variant, price, county, image.image_path, date_listed, seller_type from vehicle, image WHERE' + 
	make + model + bodyType + fuelType + transmission + minPrice + maxPrice + minYear + maxYear +
	' AND vehicle.vehicle_id = image.vehicle_id AND image.thumbnail = true ';
	if(countyIds.length > 0) {
		var counties =  'AND county IN (SELECT label FROM county WHERE id IN (' + countyIds.join(',') + ')) ';
		searchString += counties;
	}
	searchString += 'ORDER BY vehicle.vehicle_id DESC';
	if (searchString.indexOf('WHERE AND')) {
		searchString.replace('WHERE AND', 'WHERE');
	}
	// console.log(searchString);
	return searchString;
}