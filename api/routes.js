module.exports = function (app) {
	var vehicles = require('./vehicles/vehicles.js');
	var images = require('./images/images.js');
	var counties = require('./counties/counties.js');
	var parse = require('./parse.js')
	var mysql = require("mysql");
	// var connection = mysql.createConnection({
	// 	host: 'sql4.freemysqlhosting.net',
	// 	user: 'sql4101864',
	// 	password: 'uY3UijFQGE',
	// 	database: 'sql4101864'
	// });
    var connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: 'melody',
		database: 'used-motors'
	});
	connection.connect();

	// vehicles
	app.get('/api/vehicles/:order', function (request, response) {
		vehicles.getAllVehicles(request, response, connection, request.params.order);
	});
	app.post('/api/vehicles', function (request, response) {
        var countyIds = [];
        if(request.body.counties) {
            countyIds = parse.parseIdsFromJson(request.body.counties);
        }
		vehicles.getByQuery(request, response, connection, countyIds);
	});
    app.get('/api/vehicle/:vehicleId', function(request, response) {
        vehicles.getVehicleById(request, response, connection, request.params.vehicleId);
    });
	
	// images
	app.get('/api/images/:vehicleId', function (request, response) {
		images.getVehicleImages(request, response, connection, request.params.vehicleId);
	});

	// counties
	app.post('/api/counties/:countyName', function (request, response) {
		counties.insertCounty(request, response, connection, request.params.countyName);
	});
	app.get('/api/counties', function (request, response) {
		counties.getAllCountiesByProvinceDesc(request, response, connection);
	});
}

