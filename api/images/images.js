exports.getVehicleImages = function (request, response, connection, vehicleId) {
	connection.query("SELECT * FROM image WHERE vehicle_id = (?)", vehicleId, function (error, rows) {
		if (error) {
			console.log("Problem with MySQL" + error);
		} else {
			response.json(rows);
		}
	});
}