exports.insertCounty = function (request, response, connection, countyName) {
	connection.query("INSERT INTO county (county_name) VALUES (?)", countyName, function (error, info) {
		if (error) {
			console.log("Problem with MySQL" + error);
			connection.end();
		}
	});
}

exports.getAllCountiesByProvinceDesc = function (request, response, connection) {
	connection.query("SELECT id, label FROM county ORDER BY province DESC, label", function (error, rows) {
		if (error) {
			console.log("Problem with MySQL" + error);
		}
		else {
			response.json(rows);
		}
	});
}





