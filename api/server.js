var express = require("express");
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json());

// For Dev only!!!
app.use(function (request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Credentials", true);
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

require('./routes')(app);

app.listen(8000, function () {
    console.log("It's Started on PORT 8000");
});
