var parse = require('../parse');

exports.getAllVehicles = function (request, response, connection, displayOrder) {
	connection.query("SELECT vehicle.vehicle_id, year, make, model, price, county, image.image_path " +
		"FROM vehicle, image WHERE vehicle.vehicle_id = image.vehicle_id " +
		"AND image.thumbnail = true ORDER BY vehicle.vehicle_id " + displayOrder, function (error, rows) {
			if (error) {
				console.log("Problem with MySQL" + error);
			} else {
				response.status(200).json(rows);
			}
		})
}

exports.getByQuery = function (request, response, connection, countyIds) {
	var sql = parse.parseSearchModel(request.body, countyIds);
	connection.query(sql, function (error, rows) {
		if (error) {
			console.log("Problem with MySQL" + error);
		} else {
			response.status(200).json(rows);
		}
	});
}

exports.getVehicleById = function(request, response, connection, vehicleId) {
    connection.query("SELECT * FROM vehicle WHERE vehicle_id = (?)", vehicleId, function(error, rows) {
        if (error) {
			console.log("Problem with MySQL" + error);
		} else {
			response.status(200).json(rows);
		}
    });
}

 
 

