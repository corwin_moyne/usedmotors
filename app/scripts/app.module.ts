///<reference path="../../typings/_reference.ts"/>

module app {
  'use strict';

  /**
   * @ngdoc overview
   * @name app
   * @description
   * # app
   * 
   *
   * Main module of the application.
   */

  var requiredModules = [
    'ui.bootstrap',
    'ui.router',
    'ngAnimate',
    'angularjs-dropdown-multiselect',

    'app.filters',
    'app.layout',
    'app.results',
    'app.placead',
    'app.services',
    'app.user',
    'app.vehicle'
		];

  angular
    .module('app', requiredModules)
    .constant('API_ADDRESS', 'http://localhost:8000/api/')
    .constant('EDMUMDS_API_KEY', 'cdw28gx5d865rjfw6p9dzy9m');
}
