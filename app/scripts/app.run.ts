///<reference path="../../typings/_reference.ts"/>

module app {
	'use strict';

    /**
     * Calls the run phase on the module.
     */
	angular.module('app')
        .run(run);

    /** Inject the required dependencies for the run method. */
    // run.$inject = ['Service.SessionManager'];

    /**
     * Defines the run function to be executed on the module.
     */
    function run(): void {
   
    }
}