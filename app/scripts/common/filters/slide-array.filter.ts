///<reference path="../../../../typings/_reference.ts"/>

module app.filters {
	'use strict';
	
	export class ArrayFilter {
		public static Factory() {
			return(input: any[], moveNumber: number): string[] => {
				var temp = input.slice(0, moveNumber);
				var original = input.slice(moveNumber);
				var newArray = original.concat(temp);
				return newArray;
			}
		}
	}
	angular.module('app.filters').filter('ArrayFilter', [ArrayFilter.Factory]);
}