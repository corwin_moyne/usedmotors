///<reference path="../../../../../typings/_reference.ts"/>

module app.services {
	'use strict';

	export class CommonUtils {

		static $inject = ['$http', 'API_ADDRESS'];
		constructor(
			private $http: ng.IHttpService,
			private API_ADDRESS: string) {
		}
		
		/**
		 * Returns the makes from the api response passed as a parameter
		 */
		getMakesFromEdmondsApiResponse(vehicles: any): string[] {
			var makes: string[] = [];
			angular.forEach(vehicles, (value, key) => {
				makes.push(value.name);
			});
			makes.push('Enter other make');
			return makes;
		}
		
		/**
		 * Returns the models of a given make
		 */
		getModelsFromMake(vehicles: any, makeIndex: number): string[] {
			var vehicleModels: string[] = [];
			angular.forEach(vehicles[makeIndex].models, (value, key) => {
				vehicleModels.push(value.name);
			});
			vehicleModels.push('Enter other model');
			return vehicleModels;
		}

		getIrishCounties(): any {
			return this.$http.get(this.API_ADDRESS + 'counties/');
		}

		getMinYearArray(): number[] {
			var minYearArray: number[] = [];
			var date = new Date();
			for (var i = date.getFullYear() - 30; i <= date.getFullYear(); i++) {
				minYearArray.push(i);
			}
			return minYearArray;
		}
		
		getMaxYearArray(minYear: number): number[] {
			var maxYearArray: number[] = [];
			var date = new Date();
			for (var i = minYear; i <= date.getFullYear(); i++) {
				maxYearArray.push(i);
			}
			return maxYearArray;
		}
	}
	angular.module('app.services').service('app.services.CommonUtils', CommonUtils);
}