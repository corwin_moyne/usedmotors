///<reference path="../../../../../typings/_reference.ts"/>

module app.services {
	'use strict';

	export class SlideService {

		static $inject = ['$http', 'API_ADDRESS'];
		constructor(
			private $http: ng.IHttpService,
			private API_ADDRESS: any) {

		}
		
		/**
		 * Returns image paths for logo carousels
		 */
		getLogoSlides(): any[] {
			var slides = [
				{ src: 'images/logos/bmw-logo.png' },
				{ src: 'images/logos/mercedes-logo.png' },
				{ src: 'images/logos/ford-logo.png' },
				{ src: 'images/logos/chevrolet-logo.png' },
				{ src: 'images/logos/honda-logo.png' },
				{ src: 'images/logos/toyota-logo.png' },
				{ src: 'images/logos/renault-logo.png' },
				{ src: 'images/logos/peugeot-logo.png' },
				{ src: 'images/logos/fiat-logo.png' },
				{ src: 'images/logos/alfa-logo.png' }
			];
			return slides;
		}

		getThumbnailSlide(vehicleId: number): any {
			return this.$http.get(this.API_ADDRESS + 'images/' + vehicleId);
		}

		getVehicleSlides(): any[] {
			var slides = [
				{ src: 'images/thumbnail.jpg' },
				{ src: 'images/thumbnail2.jpg' },
				{ src: 'images/thumbnail3.jpg' }
			];
			return slides;
		}

	}
	angular.module('app.services').service('app.services.SlideService', SlideService);
}