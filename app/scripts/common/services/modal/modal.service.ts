///<reference path="../../../../../typings/_reference.ts"/>

module app.services {
	'use strict';

	export class ModalService {

		static $inject = ['$uibModal'];
		constructor(
			private $uibModal: ng.ui.bootstrap.IModalService) {
				
			}

		openModal(templateUrl: string, controller: string, size: string): void {
			var modalInstance = this.$uibModal.open({
				animation: true,
				templateUrl: 'scripts/' + templateUrl + '.modal.html',
				controller: controller + ' as vm',
				size: size
			});
		}
	}
	angular.module('app.services').service('app.services.ModalService', ModalService);
}
