///<reference path="../../../../../typings/_reference.ts"/>

module app.services {
    'use strict';
    
    export class VehicleIdStoreService {
        
        private vehicleId = 0;
         
        getVehicleId(): number {
            return this.vehicleId;
        }
        
        setVehicleId(vehicleId: number): void {
            this.vehicleId = vehicleId;
        }
    }
    angular.module('app.services').service('app.services.VehicleIdStoreService', app.services.VehicleIdStoreService);
}