///<reference path="../../../../../typings/_reference.ts"/>

module app.services {

  export class VehicleSearchModelStoreService {
      
      private vehicleSearchModel: app.layout.VehicleSearchModel = new app.layout.VehicleSearchModel();
      
      getVehicleSearchModel(): app.layout.VehicleSearchModel {
          return this.vehicleSearchModel;
      }
      
      setVehicleSearchModel(vehicleSearchModel: app.layout.VehicleSearchModel): void {
          this.vehicleSearchModel = vehicleSearchModel;
      }
  }
  angular.module('app.services').service('app.services.VehicleSearchModelStoreService', VehicleSearchModelStoreService);
}
