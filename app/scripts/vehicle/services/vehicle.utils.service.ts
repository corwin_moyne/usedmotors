///<reference path="../../../../typings/_reference.ts"/>

module app.vehicle {

    export class VehicleUtilsService {

        calculateAdAge(dateListed: Date): number {
            var date = new Date(dateListed.toString());
            var today = new Date();
            var timeDiff = Math.abs(today.getTime() - date.getTime());
            var difference = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return difference;
        }
    }
    angular.module('app.vehicle').service('app.vehicle.VehicleUtilsService', VehicleUtilsService);
}