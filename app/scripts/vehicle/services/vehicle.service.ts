///<reference path="../../../../typings/_reference.ts"/>

module app.vehicle {

    export class VehicleService {

        static $inject = [
            '$http',
            'API_ADDRESS',
        ];
        constructor(
            private $http: ng.IHttpService,
            private API_ADDRESS: string) {
        }

        getVehicle(vehicleId: number): any {
            return this.$http.get(this.API_ADDRESS + 'vehicle/' + vehicleId).then((response: any) => {
                return response.data[0];
            });
        }
        
        getVehicleImages(vehicleId: number): any {
            return this.$http.get(this.API_ADDRESS + 'images/' + vehicleId).then((response: any) => {
                return response.data;
            });
        }
    }
    angular.module('app.vehicle').service('app.vehicle.VehicleService', VehicleService);
}