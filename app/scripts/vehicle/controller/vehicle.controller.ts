///<reference path="../../../../typings/_reference.ts"/>

module app.vehicle {
    'use strict';

    export class VehicleController {

        static $inject = [
            'app.vehicle.VehicleUtilsService',
            'vehicle',
            'images'
        ];
        constructor(
            private vehicleUtilsService: app.vehicle.VehicleUtilsService,
            public vehicle: any,
            public images: any,
            public adAge: number) {
        }
        
        init() {
            this.getAdAge();
        }
        
        getAdAge(): void {
            this.adAge = this.vehicleUtilsService.calculateAdAge(this.vehicle.date_listed);
        }
        
    }
    angular.module('app.vehicle').controller('app.vehicle.VehicleController', VehicleController);
}