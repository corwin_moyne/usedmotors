/// <reference path="../../../../typings/_reference.ts" />

module app.results {
	'use strict';

	// Configure the routing for the application.
	angular.module('app.vehicle')
		.config(routing);
		
	/**
	 * Function to setup routing for the module.
	 */
	routing.$inject = ['$stateProvider'];
	function routing(stateProvider: angular.ui.IStateProvider) {
		stateProvider
			.state('vehicle', {
				url: '/vehicle',
				templateUrl: 'scripts/vehicle/html/vehicle.html',
				controller: 'app.vehicle.VehicleController',
				controllerAs: 'vm',
                params: {
                    id: null
                },
				resolve: {
					vehicle: resolveVehicle,
                    images: resolveImages
				}
			})
    }
    
    resolveVehicle.$inject = ['app.vehicle.VehicleService', 'app.services.VehicleIdStoreService'];
    function resolveVehicle(vehicleService: app.vehicle.VehicleService, vehicleIdStoreService: app.services.VehicleIdStoreService): ng.IPromise<any> {
        var vehicleId = vehicleIdStoreService.getVehicleId();
        return vehicleService.getVehicle(vehicleId);   
    }
    
    resolveImages.$inject = ['app.vehicle.VehicleService', 'app.services.VehicleIdStoreService'];
    function resolveImages(vehicleService: app.vehicle.VehicleService, vehicleIdStoreService: app.services.VehicleIdStoreService): ng.IPromise<any> {
        var vehicleId = vehicleIdStoreService.getVehicleId();
        return vehicleService.getVehicleImages(vehicleId);   
    }
}