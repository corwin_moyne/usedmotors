///<reference path="../../../../typings/_reference.ts"/>

module app.results {
	'use strict';
	
	export class ResultsFilter {
		public static Factory() {
			return(input: any, filter: string): any => {
                var filteredVehicles: any[] = [];
                for(var i in input) {
                    if(input[i].seller_type === filter) {
                        filteredVehicles.push(input[i]);
                    }
                }
				return filteredVehicles;
			}
		}
	}
	angular.module('app.results').filter('app.results.ResultsFilter', [ResultsFilter.Factory]);
}