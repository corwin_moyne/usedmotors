/// <reference path="../../../../typings/_reference.ts" />

module app.results {
    'use strict';

    // Configure the routing for the application.
    angular.module('app.results')
        .config(routing);
		
	/**
	 * Function to setup routing for the module.
	 */
    routing.$inject = ['$stateProvider'];
    function routing(stateProvider: angular.ui.IStateProvider) {
        stateProvider
            .state('results', {
                url: '/results',
                templateUrl: 'scripts/results/html/results.html',
                controller: 'app.results.ResultsController',
                controllerAs: 'vm',
                resolve: {
                    vehicles: resolveVehicles
                }
            })
    }

    resolveVehicles.$inject = [
        'app.results.ResultsService',
        'app.services.VehicleSearchModelStoreService'
    ];
    function resolveVehicles(
        resultsService: app.results.ResultsService,
        vehicleSearchModelStoreService: app.services.VehicleSearchModelStoreService): ng.IPromise<any> {
        var vehicleSearchModel = vehicleSearchModelStoreService.getVehicleSearchModel();
        return resultsService.getByQuery(vehicleSearchModel);
    }
}