/// <reference path="../../../../typings/_reference.ts" />

module app.results {
    'use strict';

    export class ResultsUtilsService {
        
        getVehicleOrder(order: string): any {
            var vehicleOrder = {
                order: '',
                reverse: false
            };
            if (order === 'Oldest') {
                vehicleOrder.order = 'vehicle_id';
                vehicleOrder.reverse = false;
            }
            else if (order === 'Highest Price') {
                vehicleOrder.order = 'price';
                vehicleOrder.reverse = true;
            }
            else if (order === 'Lowest Price') {
                vehicleOrder.order = 'price';
                vehicleOrder.reverse = false;
            }
            else {
                vehicleOrder.order = 'vehicle_id';
                vehicleOrder.reverse = true;
            }
            return vehicleOrder;
        }
    }
    angular.module('app.results').service('app.results.ResultsUtilsService', ResultsUtilsService);
}