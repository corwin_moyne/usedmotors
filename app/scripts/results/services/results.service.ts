/// <reference path="../../../../typings/_reference.ts" />

module app.results {
    'use strict';

    export class ResultsService {

        static $inject = [
            '$http',
            'API_ADDRESS',
            'EDMUMDS_API_KEY'
        ];
        constructor(
            private $http: ng.IHttpService,
            private API_ADDRESS: string,
            private EDMUMDS_API_KEY: string) {
        }

        getAllVehicles(order: string): any {
            return this.$http.get(this.API_ADDRESS + 'vehicles/' + order).then((response: any) => {
                return response.data;
            });
        }

        getByQuery(vehicleSearchModel: app.layout.VehicleSearchModel): any {
            return this.$http({
                method: 'POST',
                url: this.API_ADDRESS + 'vehicles/',
                data: vehicleSearchModel,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((response: any) => {
                return response.data;
            });
        }
    }
    angular.module('app.results').service('app.results.ResultsService', ResultsService);
}