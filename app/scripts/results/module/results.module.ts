/// <reference path="../../../../typings/_reference.ts" />

module app.results {
	'use strict';

	// Set the required modules for the search module component.
	var requiredModules: string[] = []; 

	// Create the search component module.
	angular.module('app.results', requiredModules);
}