/// <reference path="../../../../typings/_reference.ts" />

module app.results {
	'use strict';

	export class ResultsController {

		static $inject = [
			'app.results.ResultsService',
            'app.results.ResultsUtilsService',
            'app.services.VehicleIdStoreService',
            '$state',
            '$rootScope',
			'$scope',
            '$filter',
			'vehicles'
		];

		constructor(
			private resultsService: app.results.ResultsService,
            private resultsUtilsService: app.results.ResultsUtilsService,
            private vehicleIdStoreService: app.services.VehicleIdStoreService,
            private $state: ng.ui.IStateService,
            private $rootScope: ng.IRootScopeService,
			private $scope: ng.IScope,
            private $filter: any,
			private vehicles: any,
            public results: any,
            public order: string,
            public reverse: boolean) {
                this.results = this.vehicles;
                // console.log(this.results);
			$scope.$on('update-vehicles', (event, args) => {
				this.updateVehicles(args.order);
			});
            $scope.$on('refine-vehicles', (event, args) => {
				this.refineVehicles(args.refine);
			});
            this.watchResults();
		}
        
        watchResults(): void {
            this.$scope.$watch('results', () => {
                this.updateVehicleFoundQuantity();
            });
        }
		
		updateVehicles(order: string): void {
            var vehicleOrder = this.resultsUtilsService.getVehicleOrder(order);
            this.order = vehicleOrder.order;
            this.reverse = vehicleOrder.reverse;
		}
        
        refineVehicles(refine: string): void {
            if(refine === 'any') {
                this.results = this.vehicles;
            }
            else {
                this.results = this.$filter('app.results.ResultsFilter')(this.vehicles, refine);
            }
            this.watchResults();
        }
        
        updateVehicleFoundQuantity(): void {
            this.$rootScope.$broadcast('update-vehicle-quantity', { qty: this.results.length });   
        }
        
        getVehicle(vehicleId: number): void {
            this.vehicleIdStoreService.setVehicleId(vehicleId);
            this.$state.go('vehicle');
        }
	}
	angular.module('app.results').controller('app.results.ResultsController', ResultsController);
}