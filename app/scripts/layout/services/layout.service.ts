///<reference path="../../../../typings/_reference.ts"/>

module app.layout {

  export class LayoutService {

    static $inject = [
      '$http',
      'API_ADDRESS',
      'EDMUMDS_API_KEY'
    ];
    constructor(
      private $http: ng.IHttpService,
      private API_ADDRESS: string,
      private EDMUMDS_API_KEY: string) {
    }

    getMakeAndModelData(): any {
      var date = new Date();
      return this.$http.get(
        'https://api.edmunds.com/api/vehicle/v2/makes?state=used&year=' + date.getFullYear() + '&view=basic&fmt=json&api_key=' + this.EDMUMDS_API_KEY);
    }
  }
  angular.module('app.layout').service('app.layout.LayoutService', LayoutService);
}