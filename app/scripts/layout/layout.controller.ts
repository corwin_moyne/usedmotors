///<reference path="../../../typings/_reference.ts"/>

module app.layout {
	'use strict';

	class LayoutController {

		dataModel: app.layout.TopSearchHtmlDataModel;
		vehicleSearchModel: app.layout.VehicleSearchModel;

		static $inject = [
			'app.services.SlideService',
			'app.layout.LayoutService',
			'app.services.CommonUtils',
            'app.services.VehicleSearchModelStoreService',
            '$scope',
			'$rootScope',
			'$filter',
			'$state'
		];

		constructor(
			private slideService: app.services.SlideService,
			private vehicleService: app.layout.LayoutService,
			private commonUtils: app.services.CommonUtils,
            private vehicleSearchModelStoreService: app.services.VehicleSearchModelStoreService,
            private $scope: ng.IScope,
			private $rootScope: ng.IRootScopeService,
			private $filter: any,
			public $state: ng.ui.IStateService) {
                
			this.dataModel = new app.layout.TopSearchHtmlDataModel();
			this.vehicleSearchModel = new app.layout.VehicleSearchModel();
            $scope.$on('update-vehicle-quantity', (event, args) => {
				this.vehicleSearchModel.vehicleQty = args.qty;
			});
		}

		init(): void {
			this.getAllMakeAndModelData();
			this.getIrishCounties();
			this.getMinYearArray();
			this.getLogoSlides();
		}
		
		/** Search for vehicles */
		search(): any {
			this.dataModel.isCollapsed = true;
			this.dataModel.showOtherMake = false;
			this.dataModel.showOtherModel = false;
            this.vehicleSearchModel.sellerType = 'any';
            this.vehicleSearchModelStoreService.setVehicleSearchModel(this.vehicleSearchModel);
			this.$state.go('results', {}, { reload: true });
		}

		validateMinMaxPrice(form: any): any {
			if (this.vehicleSearchModel.minPrice != null) {
				var minPrice = parseInt(this.vehicleSearchModel.minPrice.toString());
			}
			if (this.vehicleSearchModel.maxPrice != null) {
				var maxPrice = parseInt(this.vehicleSearchModel.maxPrice.toString());
			}
			if (minPrice > 0 && maxPrice > 0 && minPrice > maxPrice) {
				form.maxPrice.$setValidity('maxPriceError', false);
			}
			else {
				form.maxPrice.$setValidity('maxPriceError', true);
			}
		}

		updateVehicles(order: string): any {
			this.$rootScope.$broadcast('update-vehicles', { order: order });
		}
        
        refineVehicles(): any {
			this.$rootScope.$broadcast('refine-vehicles', { refine: this.vehicleSearchModel.sellerType });
		}

		getAllMakeAndModelData(): void {
			this.vehicleService.getMakeAndModelData().then((response: any) => {
				this.dataModel.vehicles = response.data.makes;
				this.dataModel.makes = this.getMakesFromEdmondsApiResponse(this.dataModel.vehicles);
			});
		}

		getMakesFromEdmondsApiResponse(vehicles: any): string[] {
			return this.commonUtils.getMakesFromEdmondsApiResponse(vehicles);
		}

		getModelsFromMake(make: string): void {
			if (make) {
				if (make.indexOf('Enter') != -1) {
					this.dataModel.showOtherMake = true;
					this.dataModel.showOtherModel = true;
				}
				else {
					this.dataModel.showOtherMake = false;
					this.dataModel.showOtherModel = false;
					var makeIndex = this.dataModel.makes.indexOf(make);
					this.dataModel.models = this.commonUtils.getModelsFromMake(this.dataModel.vehicles, makeIndex);
				}
			}
		}

		chooseOtherModel(model: string): void {
			if (model) {
				if (model.indexOf('Enter') != -1) {
					this.dataModel.showOtherModel = true;
				}
				else {
					this.dataModel.showOtherModel = false;
				}
			}
		}

		getIrishCounties(): void {
			this.commonUtils.getIrishCounties().then((response: any) => {
				this.dataModel.counties = response.data;
			});
		}

		getMinYearArray(): void {
			this.dataModel.minYearArray = this.commonUtils.getMinYearArray();
		}

		getMaxYearArray(minYear: number): void {
			this.dataModel.maxYearArray = this.commonUtils.getMaxYearArray(minYear);
		}

		getLogoSlides(): void {
			this.dataModel.slides = this.slideService.getLogoSlides();
			this.dataModel.slides2 = this.$filter('ArrayFilter')(this.dataModel.slides, 1);
			this.dataModel.slides3 = this.$filter('ArrayFilter')(this.dataModel.slides, 2);
		}
	}
	angular.module('app.layout').controller('app.layout.LayoutController', LayoutController);
}