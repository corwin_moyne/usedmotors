///<reference path="../../../../typings/_reference.ts"/>

module app.layout {
	'use strict';
	
	export class TopSearchHtmlDataModel {
		slideIndex: number = 0;
		slideInterval: number = 10000;
		minYearArray: number[] = [];
		maxYearArray: number[] = [];
		vehicles: any[] = [];
		slides: string[] = [];
		slides2: string[] = [];
		slides3: string[] = [];
		makes: string[] = [];
		models: string[] = [];
		counties: string[] = [];
		isCollapsed: boolean = true;
		showOtherMake: boolean = false;
		showOtherModel: boolean = false;
		multipleCountySelect = { enableSearch: true, scrollableHeight: '300px', scrollable: true };
		multipleCountySelectText = { buttonDefaultText: 'Any County' };
	}
}