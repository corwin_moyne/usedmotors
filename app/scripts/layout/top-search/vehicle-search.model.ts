///<reference path="../../../../typings/_reference.ts"/>

module app.layout {
	'use strict';
	
	export class VehicleSearchModel {
		make: string = null;
		model: string = null;
		bodyType: string = null;
		fuelType: string = null;
		transmission: string = null;
		keyWord: string = null;
		minPrice: number = null;
		maxPrice: number = null;
		minYear: number = null;
		maxYear: number = null;
		counties: any[] = [];
        sellerType: string = 'any';
        vehicleQty: number = null;
	}
}