///<reference path="../../../../typings/_reference.ts"/>

module app.layout {
	'use strict';

	interface ITopNavController {

	}

	class TopNavController implements ITopNavController {

		static $inject = ['app.services.ModalService', '$state'];
		constructor(
			private modalService: app.services.ModalService,
			private $state: ng.ui.IStateService) {
		}

		openLoginModal(): void {
			this.modalService.openModal('user/login', 'app.user.LoginModalController', 'sm');
		}
		
		openSignupModal(): void {
			this.modalService.openModal('user/signup', 'app.user.SignupModalController', 'sm');
		}
		
		placeAd(): void {
			//Must be logged in!
			this.$state.go('placead');
		}
	}
	angular.module('app.layout').controller('app.layout.TopNavController', TopNavController);
}