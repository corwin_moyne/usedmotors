/// <reference path="../../../typings/_reference.ts" />

module app.placead {
	'use strict';

	// Configure the routing for the application.
	angular.module('app.placead')
		.config(routing);
		
	/**
	 * Function to setup routing for the module.
	 */
	routing.$inject = ['$stateProvider'];
	function routing(stateProvider: angular.ui.IStateProvider) {
		stateProvider
			.state('placead', {
				url: '/placead',
				templateUrl: 'scripts/place-ad/place-ad.html',
				controller: 'app.placead.PlaceAdController',
				controllerAs: 'vm'
				// resolve: {
				// 	// data: resolveData,
				// 	// config: resolveConfig
				// }
			})
    }


}