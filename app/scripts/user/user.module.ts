/// <reference path="../../../typings/_reference.ts" />

module app.user {
	'use strict';

	// Set the required modules for the module component.
	var requiredModules: string[] = []; 

	// Create the component module.
	angular.module('app.user', requiredModules);
}