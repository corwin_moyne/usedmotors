///<reference path="../../../typings/_reference.ts"/>

module app.user {
	'use strict';

	interface ISignupModalController {
		login(): void;
		cancel(): void;
	}

	class SignupModalController implements ISignupModalController {

		static $inject = ['$uibModalInstance', 'app.services.ModalService'];
		constructor(
			private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance,
			private modalService: app.services.ModalService) {
		}
		
		/** Open login modal */
		login(): void {
			this.cancel();
			this.modalService.openModal('user/login', 'app.user.LoginModalController', 'sm');
		}

		cancel(): void {
			this.$uibModalInstance.dismiss('cancel');
		}
	}
	angular.module('app.user').controller('app.user.SignupModalController', SignupModalController);
}