///<reference path="../../../typings/_reference.ts"/>

module app.user {
	'use strict';

	interface ILoginModalController {
		signUp(): void;
		cancel(): void;
	}

	class LoginModalController implements ILoginModalController {

		static $inject = ['$uibModalInstance', 'app.services.ModalService'];
		constructor(
			private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance,
			private modalService: app.services.ModalService) {
		}
		
		/** Open sign up modal */
		signUp(): void {
			this.cancel();
			this.modalService.openModal('user/signup', 'app.user.SignupModalController', 'sm');
		}

		cancel(): void {
			this.$uibModalInstance.dismiss('cancel');
		}
	}
	angular.module('app.user').controller('app.user.LoginModalController', LoginModalController);
}