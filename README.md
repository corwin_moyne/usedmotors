# used-motors

## Build & development

To run the backend server, run 'node api/server.js (must have mysql running)

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
